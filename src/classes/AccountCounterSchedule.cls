global class AccountCounterSchedule implements Schedulable{
	public static String CRON_EXP = '0 1 * * * ? ';
    global void execute(SchedulableContext ctx) {
        AccountCounterBatch b = new AccountCounterBatch();
        Database.executeBatch(b);
    }
}