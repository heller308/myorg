@isTest public class TestVerifyDate {
    static Date d =Date.newInstance(2012, 08, 13) ;
    static Date d1 =Date.newInstance(2012, 09, 10) ;
    static Date d2 =Date.newInstance(2012, 09, 15) ;
    static Date d3 =Date.newInstance(2012, 08, 31) ;
 @isTest static void withinRange()
 {
     Date res= VerifyDate.CheckDates(d,d1);
     System.assertEquals(d1, res);
 }
    @isTest static void notInRange()
 {
     Date res= VerifyDate.CheckDates(d,d2);
     System.assertEquals(d3, res);
 }
}