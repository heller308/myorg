trigger ClosedOpportunityTrigger on Opportunity (before insert,before update) {
    List<Task> tasks=new List<task>();
    for(Opportunity op:trigger.new){
        if(op.stageName=='Closed Won'){
            Task t=new Task();
            t.Subject='Follow Up Test Task';
            t.WhatId=op.Id;
           tasks.add(t);
        }
    }
    if(!tasks.isEmpty()){
        insert(tasks);
    }
}