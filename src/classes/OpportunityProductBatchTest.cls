@isTest
private class OpportunityProductBatchTest {

	 static testMethod void test() {

        // The query used by the batch job.
        	String query ='SELECT ID,name,primary__c,active__c,opportunity__c '+ 
		'from opportunity_product__c '+ 
		'WHERE Opportunity__c=\'0062800000FzZhq\'';

       // Create some test merchandise items to be deleted
       //   by the batch job.
       Opportunity_Product__c[] ml = new List<Opportunity_Product__c>();
       for (Integer i=0;i<10;i++) {
           Opportunity_Product__c m = new Opportunity_Product__c(
               Name='Product ' + i,
               primary__c=true,
               active__c=true,
               Opportunity__c='0062800000FzZhq');
           ml.add(m);
       }
       insert ml;

       Test.startTest();
       OpportunityProductBatch c = new OpportunityProductBatch(query);
       Database.executeBatch(c);
       Test.stopTest();

       //&nbsp;Verify&nbsp;merchandise&nbsp;items&nbsp;got&nbsp;deleted 
       Integer i=[SELECT COUNT() FROM Opportunity_Product__c where primary__c=true AND Opportunity__c='0062800000FzZhq'];
       System.assertEquals(i,1);
	}

}