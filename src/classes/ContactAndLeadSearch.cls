public class ContactAndLeadSearch {
    public static List<List<sObject>> searchContactsAndLeads(String a){
        List<List<sObject>> s=[FIND :a IN Name Fields RETURNING Contact(lastName), lead(FirstName,LastName)];
        return s;
    }
}