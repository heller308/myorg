public class opportunityProductController {
    
    public String search                                    {get;set;}
    public List<Opportunity_Product__c> opportunityProducts            {get;set;}
    
    public opportunityProductController(){
        opportunityProducts = new List<Opportunity_Product__c>();
    }
    public void searchPros(){
        Id oppId;
        try{
            oppId = [SELECT ID 
                     FROM Opportunity 
                        WHERE name=:search LIMIT 1].ID;
        }catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'No Opportunity found with this name.'));
        }
        if(oppId != null){
            opportunityProducts = new List<Opportunity_Product__c>([SELECT ID,NAME,primary__c,active__c 
                                                                                        FROM Opportunity_Product__c 
                                                                                        WHERE Opportunity__c=:oppId LIMIT 20]);
        }else{
            opportunityProducts = new List<Opportunity_Product__c>();
        }    
    }
    public void updatePros(){
        if(!opportunityProducts.isEmpty()){
            update opportunityProducts;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Updated Successfully.'));
        }
    }
}