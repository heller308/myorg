trigger OpportunityProduct on Opportunity_Product__c (before insert, after delete,before update,after undelete) {
   if(Trigger.isAfter){
        if(Trigger.isDelete){
          OpportunityProductHandler.afterProductDelete(Trigger.old); 
        } else if(Trigger.isunDelete){
          OpportunityProductHandler.afterProductUnDelete(Trigger.New); 
        }
    }
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            OpportunityProductHandler.beforeProductInsert(Trigger.New);
        }
        else if(Trigger.isUpdate){
            OpportunityProductHandler.beforeProductUpdate(Trigger.New,Trigger.oldMap);
        }
    }

}