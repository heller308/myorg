public class RandomContactFactory {
    public static List<Contact> generateRandomContacts(integer numContacts,String lastName){
        List<Contact> contacts= new List<Contact>();
        for(integer i=0;i<numContacts;i++){
            Contact contactObj = new Contact();
            contactObj.LastName=lastName;
            contactObj.FirstName='Test '+i;
            contacts.add(contactObj);
        }
        return contacts;
    }
}