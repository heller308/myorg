public class OpportunityProductHandler{
    public static Boolean flag=false;
    public static void beforeProductInsert(List<Opportunity_Product__c> newProductList){
        
            Set<Id> OpportunityIds = new Set<Id>(); //set for Opportunity__c for active and primary 
            for(Opportunity_product__C oppObj : newProductList){
                if(OpportunityIds.contains(oppObj.Opportunity__c)){
                    oppObj.primary__c = false;
                }
                else if(oppObj.primary__c==true && oppObj.active__c==true ){
                    OpportunityIds.add(oppObj.Opportunity__c);
                }
            }
            //List contains previous primary products 
            List<Opportunity_Product__c> prevPrimary = new List<Opportunity_Product__c>([SELECT ID,primary__c FROM Opportunity_Product__c 
                                                                                            WHERE  primary__c=true 
                                                                                            AND Opportunity__c in :OpportunityIds]);
            for(Opportunity_Product__c oppObj : prevPrimary){
                oppObj.primary__c=false;
            }
            if(!prevPrimary.isEmpty()){
                flag=true;
                update prevPrimary;
            }
    }
    
    
     public static void beforeProductUpdate(List<Opportunity_Product__c> updatedProductList,Map<Id,Opportunity_Product__c> oldMap){
        Set<Id> OpportunityIds = new Set<Id>();
        Set<Id> productIds = new Set<Id>();
        for(Opportunity_product__C oppObj : updatedProductList){
            Opportunity_product__C  oldoppObj = oldMap.get(oppObj.Id);
            if(oldoppObj.primary__c==true && oppObj.primary__c==false && flag==false){
                 oppObj.addError('You can not make a product non primary');//when update primary to non-primary
            }
            if(OpportunityIds.contains(oppObj.Opportunity__c) ){
                oppObj.primary__c = false;
            }
            else if(oppObj.primary__c==true && oppObj.active__c==true ){
                OpportunityIds.add(oppObj.Opportunity__c);
            }
            productIds.add(oppObj.ID);
        }
        //List for previous primary products
        List<Opportunity_Product__c> prevPrimary = new List<Opportunity_Product__c>([SELECT ID,primary__c FROM Opportunity_Product__c 
                                                                                        WHERE  primary__c=true AND Id not in :productIds 
                                                                                        AND Opportunity__c in :OpportunityIds]);
        for(Opportunity_Product__c oppObj : prevPrimary){
            oppObj.primary__c=false; //make Primary to non-primary
        }
        System.debug('Prev Primary'+prevPrimary);
        if(!prevPrimary.isEmpty() && flag==false){
            flag=true;
            update prevPrimary;
            
        }
    }
    
    
     public static void afterProductDelete(List<Opportunity_Product__c> deletedProductList){
         //if primary product is deleted
        Set<ID> oppProductIds = new Set<ID>();
        for(Opportunity_Product__c oppObj : deletedProductList){
            if(oppObj.primary__c==true){ 
                oppProductIds.add(oppObj.Opportunity__c);
            }
        }
        System.debug('OppIdSet of deleted Product '+oppProductIds);
        //List of active and non-primary products
        List<Opportunity_Product__c> oppList1 = new List<Opportunity_Product__c>([Select Id,Name,active__c,primary__c,Opportunity__c 
                                                        from Opportunity_Product__c 
                                                        WHERE Opportunity__c in :oppProductIds AND Id NOT IN :Trigger.Old AND active__c=true]);
        if(oppList1.size()<1){
            for(Opportunity_Product__c oppObj : deletedProductList){
                if(oppObj.primary__c==true){
            oppObj.addError('You can not delete the last primary product');
                }
            }
        }
        Map<ID,Set<Id>> remainedproductMap = new Map<Id,Set<Id>>(); //Map for remaining active products
        for(Opportunity_Product__c oppObj : oppList1){
            Set<Id> ids;
                if(remainedproductMap.containsKey(oppObj.Opportunity__c)) {
                    ids = remainedproductMap.get(oppObj.Opportunity__c);
                }
                else
                {
                    ids = new Set<Id>();
                }
                ids.add(oppObj.Id);
                remainedproductMap.put(oppObj.Opportunity__c, ids);
        }
        System.debug('Map is '+remainedproductMap);
        List<Opportunity_Product__c> updateProduct = new List<Opportunity_Product__c>();
        for(Id ids :remainedproductMap.keyset()){
                Set<Id> setIds = remainedproductMap.get(ids);
                Opportunity_Product__c oppObj = new Opportunity_Product__c();
                for(Id oppProductObj : setIds){
                    oppObj.Id = oppProductObj;
                    break;
        		}
                oppObj.Primary__c=true; //make another product as primary
                updateProduct.add(oppObj);
        	}
    	if(!updateProduct.isEmpty()){
    	    update updateProduct;
    	}
    }
    
    
     public static void afterProductUndelete(List<Opportunity_Product__c> unDeleteProductList){
         //if primary is undeleted
         System.debug('Undelete List '+unDeleteProductList);
        Set<Id> OpportunityIds = new Set<Id>();
        Set<Id> productIds = new Set<Id>();
        for(Opportunity_product__C oppObj : unDeleteProductList){
            if(!OpportunityIds.contains(oppObj.Opportunity__c) && oppObj.primary__c==true){
                OpportunityIds.add(oppObj.Opportunity__c);
                productIds.add(oppObj.ID);
            }
        }
        //list of current primary
        List<Opportunity_Product__c> prevPrimary = new List<Opportunity_Product__c>([SELECT ID,primary__c FROM Opportunity_Product__c 
                                                                                        WHERE  primary__c=true AND ID not in:productIds 
                                                                                        AND Opportunity__c in :OpportunityIds]);
        //list of current primary to remove
        for(Opportunity_Product__c oppObj : prevPrimary){
                oppObj.primary__c=false;
            }
            if(!prevPrimary.isEmpty()){
                flag=true;
                update prevPrimary;
            }
    }
    
    public static void BatchProductUpdate(List<Opportunity_Product__c> newProductList){
            Set<Id> productIds = new Set<Id>();
            Set<Id> OpportunityIds = new Set<Id>(); //set for Opportunity__c for active and primary 
            for(Opportunity_product__C oppObj : newProductList){
                if(OpportunityIds.contains(oppObj.Opportunity__c)){
                    oppObj.primary__c = false;
                }
                else if(oppObj.primary__c==true && oppObj.active__c==true ){
                    OpportunityIds.add(oppObj.Opportunity__c);
                    productIds.add(oppObj.ID);
                }
            }
            List<Opportunity_Product__c> makePrimary = new List<Opportunity_Product__c>([SELECT ID,primary__c FROM Opportunity_Product__c 
                                                                                           WHERE  Opportunity__c in :OpportunityIds
                                                                                            AND ID  in :productIds]);
            for(Opportunity_Product__c oppObj : makePrimary){
                oppObj.primary__c=true;
            }
            if(!makePrimary.isEmpty()){
                flag=true;
                update makePrimary;
            }                                                                               
            //List contains previous primary products 
            List<Opportunity_Product__c> prevPrimary = new List<Opportunity_Product__c>([SELECT ID,primary__c FROM Opportunity_Product__c 
                                                                                            WHERE  primary__c=true 
                                                                                            AND Opportunity__c in :OpportunityIds
                                                                                            AND ID not in :productIds]);
            for(Opportunity_Product__c oppObj : prevPrimary){
                oppObj.primary__c=false;
            }
            if(!prevPrimary.isEmpty()){
                flag=true;
                update prevPrimary;
            }
    }
}