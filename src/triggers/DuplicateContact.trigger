trigger DuplicateContact on Contact (before insert, before update, after delete, after undelete) {
    if(Trigger.isInsert){
        Set<String> lnames= new Set<String>();
       // List<Contact> contactList = new List<Contact>();
        for(Contact contactObj :Trigger.New){
            
            if(lnames.add(contactObj.lastName)){
                contactObj.Duplicate__c = false;
            }else{
                contactObj.Duplicate__c=true;
            }
        }
        System.debug('Set Values ' + lnames);
        List<Contact> contactList = new List<Contact>([SELECT LastName FROM Contact 
                                                       WHERE LastNAme in :lnames AND duplicate__c=false]);
        Set<String> prevLnames = new Set<String>();
        for(Contact contactObj : ContactList){
            prevLnames.add(contactObj.LastName);
        }
        for(Contact contactObj :Trigger.New){
            if(contactObj.Duplicate__c==false && prevLnames.contains(contactObj.LastName)){
                contactObj.Duplicate__c=true;
            }
        }
    }
    if(Trigger.isUpdate){
        Set<String> updatedNames = new Set<String>();
        Set<String> uniqueNames  = new Set<String>();
        for(Contact contactObj : Trigger.new){
            Contact oldContactObj = Trigger.oldMap.get(contactObj.Id);
            if(oldContactObj.LastName != contactObj.LastName){
                updatedNames.add(contactObj.LastName);
                if(contactObj.Duplicate__c==false){
                    uniqueNames.add(oldContactObj.LastName);
                }
            }
    	}
    	System.debug('name Change '+updatedNames);
    	System.debug('Unique Names '+uniqueNames);
        List<Contact> namesList = new List<Contact>([SELECT LastName from Contact WHERE LastName in :updatedNames AND Duplicate__c=false]);
        system.debug('Remaining Contact with Uncheked of updatedNames'+namesList);
        Set<String> prevNames = new Set<String>();
        for(Contact contactObj :namesList){
            prevNames.add(contactObj.LastName);
        }
        if(prevNames.size()>0){
            for(Contact contactObj :Trigger.New){
                if(prevNames.contains(contactObj.LastName)){
                    contactObj.Duplicate__c = true;
                }
                else{
                    contactObj.Duplicate__c=false;
                }
            }
        }
        System.debug('After PrevNames '+Trigger.New);
        List<Contact> uniqueNameList = new List<Contact>([SELECT ID, LastName, createdDate FROM Contact
                     WHERE LastName in :uniqueNames AND Duplicate__c=true ORDER BY createdDate]);
        system.debug('Remaining Contact with Uncheked of UniqueNames'+uniqueNameList);
        Map<String,Set<Id>> contactMap = new Map<String,Set<Id>>();
        for(Contact contactObj :uniqueNameList){
            Set<Id> ids;
            if(contactMap.containsKey(contactObj.LastName)) {
                ids = contactMap.get(contactObj.LastName);
            }
            else
            {
                ids = new Set<Id>();
            }
            ids.add(contactObj.Id);
            contactMap.put(contactObj.LastName, ids);
        }
        system.debug('contactMap '+contactMap);
        List<contact> updateContact = new List<contact>();
        for(String ln :contactMap.keyset()){
            Set<Id> setIds = contactMap.get(ln);
            contact conObj = new Contact();
            for(Id contactId : setIds){
                conObj.Id = contactId;
                break;
    		}
            conObj.Duplicate__c=false;
            updateContact.add(conObj);
            
    	}
    	if(!updateContact.isEmpty()){
    	    update updateContact;
    	    System.debug('updated Contacts '+updateContact);
    	}
    	
	}
	if(Trigger.isDelete){
	    Set<String> nonDuplicate = new Set<String>();
	    for(Contact contactObj : Trigger.Old){
	        if(contactObj.Duplicate__c==false){
	            nonDuplicate.add(contactObj.LastName);
	        }
	    }
	    List<Contact> remainedContact = new List<Contact>([SELECT ID,LastName FROM Contact 
	                                WHERE LastName in :nonDuplicate 
	                                and Duplicate__c=true ORDER BY createdDate]);
	    Map<String,Set<Id>> contactMap = new Map<String,Set<Id>>();
        for(Contact contactObj :remainedContact){
            Set<Id> ids;
            if(contactMap.containsKey(contactObj.LastName)) {
                ids = contactMap.get(contactObj.LastName);
            }
            else
            {
                ids = new Set<Id>();
            }
            ids.add(contactObj.Id);
            contactMap.put(contactObj.LastName, ids);
        }
        system.debug('contactMap '+contactMap);
        List<contact> updateContact = new List<contact>();
        for(String ln :contactMap.keyset()){
            Set<Id> setIds = contactMap.get(ln);
            contact conObj = new Contact();
            for(Id contactId : setIds){
                conObj.Id = contactId;
                break;
    		}
            conObj.Duplicate__c=false;
            updateContact.add(conObj);
            
    	}
    	if(!updateContact.isEmpty()){
    	    update updateContact;
    	}
	}
	if(Trigger.isUndelete){
	    Set<String> lastNames = new Set<String>();
	    Set<id> undeletedContactIds = new Set<id>();
	    for(Contact contactObj : Trigger.New){
	        if(contactObj.Duplicate__c==false){
	            lastNames.add(contactObj.LastName);
	            undeletedContactIds.add(contactObj.Id);
	        }
	    }
	    List<Contact> remainedContact = new List<Contact>([SELECT ID,LastName FROM Contact 
	                                WHERE LastName in :lastNames 
	                                and id not in:undeletedContactIds]);
	   system.debug('remainedContact : ' + remainedContact);   
        List<contact> updateContact = new List<Contact>();
	    for(contact contactObj :remainedContact){
            contactObj.Duplicate__c=true;
            updateContact.add(contactObj);
            
    	}
    	if(!updateContact.isEmpty()){
    	    update updateContact;
    	}
	}
}