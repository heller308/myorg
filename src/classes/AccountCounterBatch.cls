global class AccountCounterBatch implements Database.Batchable<sObject> {
	global  String query='SELECT ID,counter__c from Account';

    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc,List<Account> data){
       List<Account> accountList = new List<Account>();
        for(Account accObj : data){
            accObj.counter__c=accObj.counter__c+1;
            accountList.add(accObj);
        }
        update accountList;
    }
    
    global void finish(Database.BatchableContext bc){
    }
}